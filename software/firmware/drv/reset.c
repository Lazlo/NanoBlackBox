#include "reset.h"
#include <avr/io.h>

uint8_t reset_get(void)
{
#if defined(__AVR_ATmega1284P__)
	return MCUSR;
#else
	return MCUCSR;
#endif
}

void reset_cause_print(void (*fp_puts)(char *s))
{
	uint8_t reset_cause = reset_get();

	(*fp_puts)("RST");
	if (reset_cause & (1 << 0)) {
		(*fp_puts)(" POR");
	}
	if (reset_cause & (1 << 1)) {
		(*fp_puts)(" EXTR");
	}
	if (reset_cause & (1 << 2)) {
		(*fp_puts)(" BOR");
	}
	if (reset_cause & (1 << 3)) {
		(*fp_puts)(" WDR");
	}
	(*fp_puts)("\r\n");
}
