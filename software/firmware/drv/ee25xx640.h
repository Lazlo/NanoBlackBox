#pragma once

#include <stdint.h>

void ee25xx640_init(void);
void ee25xx640_read(const uint16_t addr, uint8_t *dest, const uint8_t len);
void ee25xx640_write(const uint16_t addr, uint8_t *src, const uint8_t len);
