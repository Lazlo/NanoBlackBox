#include "uart.h"
#include <avr/io.h>

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega644__)

#define UART_BAUDRATE_REGH	UBRR0H
#define UART_BAUDRATE_REGL	UBRR0L
#define UART_SCA_REG		UCSR0A
#define UART_DRE_BIT		UDRE0
#define UART_SCB_REG		UCSR0B
#define UART_TXEN_BIT		TXEN0
#define UART_DATA_REG		UDR0

#elif defined(__AVR_ATmega8__)

#define UART_BAUDRATE_REGH	UBRRH
#define UART_BAUDRATE_REGL	UBRRL
#define UART_SCA_REG		UCSRA
#define UART_DRE_BIT		UDRE
#define UART_SCB_REG		UCSRB
#define UART_TXEN_BIT		TXEN
#define UART_DATA_REG		UDR

#else

#error No register mapping for selected target!

#endif

static inline void uart_setbaudrate(const uint32_t baud)
{
	uint16_t brr_val = F_CPU / baud / 16;
	UART_BAUDRATE_REGH = brr_val >> 8;
	UART_BAUDRATE_REGL = brr_val & 0xFF;
}

static inline void uart_enable(void)
{
	UART_SCB_REG |= (1 << UART_TXEN_BIT);
}

void uart_init(const uint32_t baud)
{
	uart_setbaudrate(baud);
	uart_enable();
}

void uart_putc(char c)
{
	while (!(UART_SCA_REG & (1 << UART_DRE_BIT)))
		;
	UART_DATA_REG = c;
}

void uart_puts(char *s)
{
	while (*s)
		uart_putc(*s++);
}
