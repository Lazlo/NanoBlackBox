#pragma once

#include <stdint.h>

struct gpio_struct {
	uint8_t offset;
	volatile uint8_t *direction_reg;
	volatile uint8_t *output_reg;
	volatile uint8_t *input_reg;
};

inline void gpio_setdirection(const struct gpio_struct *g, const uint8_t direction)
{
	uint8_t mask = (1 << g->offset);
	if (direction)
		*g->direction_reg |= mask;
	else
		*g->direction_reg &= ~mask;
}

inline void gpio_write(const struct gpio_struct *g, const uint8_t bit)
{
	uint8_t mask = (1 << g->offset);
	if (bit)
		*g->output_reg |= mask;
	else
		*g->output_reg &= ~mask;
}

inline uint8_t gpio_read(const struct gpio_struct *g)
{
	uint8_t mask = (1 << g->offset);
	return *g->input_reg & mask ? 1 : 0;
}
