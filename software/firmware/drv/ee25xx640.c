#include "ee25xx640.h"
#include "config_pins.h"
#include "spi.h"

enum ee25xx640_op {
	WRSR = 1,
	WRITE,
	READ,
	WRDI,
	RDSR,
	WREN
};

static void ee25xx640_select(const uint8_t select)
{
	if (select)
		PIN_25LC640_CS_PORT &= ~(1 << PIN_25LC640_CS);
	else
		PIN_25LC640_CS_PORT |= (1 << PIN_25LC640_CS);
}

static void ee25xx640_write_enable(const uint8_t enable)
{
	ee25xx640_select(1);
	spi_trx(enable ? WREN : WRDI);
	ee25xx640_select(0);
}

void ee25xx640_init(void)
{
	PIN_25LC640_CS_DDR |= (1 << PIN_25LC640_CS);
	PIN_25LC640_CS_PORT |= (1 << PIN_25LC640_CS);
}

void ee25xx640_read(const uint16_t addr, uint8_t *dest, const uint8_t len)
{
	uint8_t i;

	ee25xx640_select(1);
	spi_trx(READ);
	spi_trx(addr >> 8);
	spi_trx(addr & 0xFF);
	for (i = 0; i < len; i++)
		dest[i] = spi_trx(0);
	ee25xx640_select(0);
}

void ee25xx640_write(const uint16_t addr, uint8_t *src, const uint8_t len)
{
	uint8_t i;

	ee25xx640_write_enable(1);

	ee25xx640_select(1);
	spi_trx(WRITE);
	spi_trx(addr >> 8);
	spi_trx(addr & 0xFF);
	for (i = 0; i < len; i++)
		spi_trx(src[i]);
	ee25xx640_select(0);

	/* write enable latch is reset by chip itself
	 * after the write instruction has been executed */
}
