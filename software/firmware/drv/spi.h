#pragma once

#include <stdint.h>

void spi_init(void);
uint8_t spi_trx(const uint8_t byte);
