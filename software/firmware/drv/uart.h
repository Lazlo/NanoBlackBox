#pragma once

#include <stdint.h>

void uart_init(const uint32_t baud);
void uart_putc(char c);
void uart_puts(char *s);
