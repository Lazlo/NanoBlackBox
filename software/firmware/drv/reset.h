#pragma once

#include <stdint.h>

uint8_t reset_get(void);
void reset_cause_print(void (*fp_puts)(char *));
