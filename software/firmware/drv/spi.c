#include "spi.h"
#include "config_pins.h"
#include <avr/io.h>

void spi_init(void)
{
	PIN_SPI_MOSI_DDR |= (1 << PIN_SPI_MOSI);
	PIN_SPI_SCK_DDR |= (1 << PIN_SPI_SCK);

	SPCR = (1 << SPE)|(1 << MSTR)|(1 << SPR0);
}

uint8_t spi_trx(const uint8_t byte)
{
	uint8_t tout = 0xFF;

	SPDR = byte;
	while (!(SPSR & (1 << SPIF)) && tout--)
		;
	return SPDR;
}
