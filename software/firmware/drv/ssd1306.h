#pragma once

#include <stdint.h>

void ssd1306_init(const uint8_t sla);
void ssd1306_clear(void);
void ssd1306_display(void);
void ssd1306_setpixel(const uint8_t x, const uint8_t y, const uint8_t state);
void ssd1306_drawchar(const uint8_t x, const uint8_t y, const char c);
void ssd1306_write(const uint8_t x, const uint8_t y, char *s);
#if 0
void ssd1306_drawrect(const uint8_t x, const uint8_t y,
			const uint8_t width, const uint8_t height);
#endif
void ssd1306_setcontrast(const uint8_t v);
