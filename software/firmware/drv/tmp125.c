#include "tmp125.h"
#include "config_pins.h"
#include "spi.h"

#define TMP125_OFFSET		5
#define TMP125_MASK		0x1FF
#define TMP125_RESOLUTION	0.25

struct tmp125_struct {
	uint8_t msb;
	uint8_t lsb;
	uint8_t sign;
};

static struct tmp125_struct state;

static void tmp125_select(const uint8_t select)
{
	if (select)
		PIN_TMP125_CS_PORT &= ~(1 << PIN_TMP125_CS);
	else
		PIN_TMP125_CS_PORT |= (1 << PIN_TMP125_CS);
}

void tmp125_init(void)
{
	PIN_TMP125_CS_DDR |= (1 << PIN_TMP125_CS);
	PIN_TMP125_CS_PORT |= (1 << PIN_TMP125_CS);
}

int16_t tmp125_read(void)
{
	int16_t temp = 0;

	tmp125_select(1);
	state.msb = spi_trx(0);
	state.lsb = spi_trx(0);
	tmp125_select(0);

	temp |= (state.msb << 8);
	temp |= state.lsb;
	temp = (temp >> TMP125_OFFSET);

	state.sign = temp & (1 << 9) ? 1 : 0;

	if (state.sign) {
		/* remove sign bit from 10th position */
		temp &= ~(1 << 9);
		/* set it again at the 16th position */
		temp |= (1 << 15);
	}

	return temp * TMP125_RESOLUTION;
}

void tmp125_tostr(int16_t c, char *s)
{
	uint8_t sign = c & (1 << 15) ? 1 : 0;

	if (sign)
		c = c * -1;
	s[0] = sign ? '-' : ' ';
	s[1] = '0' + c / 100;
	s[2] = '0' + (c % 100) / 10;
	s[3] = '0' + c % 10;
	s[4] = '\0';
}
