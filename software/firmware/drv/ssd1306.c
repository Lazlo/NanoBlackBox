#include "ssd1306.h"
#include "config.h"
#include "i2c.h"
#include "string.h"
#include "glcdfont.c"
#include <avr/pgmspace.h>

#define SSD1306_SWITCHCAPVCC		0x02
#define SSD1306_MEMORYMODE		0x20
#define SSD1306_COLUMNADDR		0x21
#define SSD1306_PAGEADDR		0x22
#define SSD1306_DEACTIVATE_SCROLL	0x2E
#define SSD1306_SETSTARTLINE		0x40
#define SSD1306_SETCONTRAST		0x81
#define SSD1306_CHARGEPUMP		0x8D
#define SSD1306_SEGREMAP		0xA0
#define SSD1306_DISPLAYALLON_RESUME	0xA4
#define SSD1306_NORMALDISPLAY		0xA6
#define SSD1306_SETMULTIPLEX		0xA8
#define SSD1306_DISPLAYOFF		0xAE
#define SSD1306_DISPLAYON		0xAF
#define SSD1306_COMSCANDEC		0xC8
#define SSD1306_SETDISPLAYOFFSET	0xD3
#define SSD1306_SETDISPLAYCLOCKDIV	0xD5
#define SSD1306_SETPRECHARGE		0xD9
#define SSD1306_SETCOMPINS		0xDA
#define SSD1306_SETVCOMDETECT		0xDB

#define SSD1306_COLUMN_START_ADDR	0
#define SSD1306_COLUMN_END_ADDR		(SSD1306_LCDWIDTH - 1)

#define SSD1306_PAGE_START_ADDR		0

#if SSD1306_LCDHEIGHT == 32

#define SSD1306_PAGE_END_ADDR		3

#elif SSD1306_LCDHEIGHT == 64

#define SSD1306_PAGE_END_ADDR		7

#endif

#define SSD1306_BUFSIZE (SSD1306_LCDHEIGHT * SSD1306_LCDWIDTH / 8)

static uint8_t ssd1306_buffer[SSD1306_BUFSIZE];
static uint8_t ssd1306_sla;

static void ssd1306_clearbuf(void)
{
	my_memset(ssd1306_buffer, 0, SSD1306_BUFSIZE);
}

static void ssd1306_command(uint8_t b)
{
	uint8_t data[] = {0x00,b};
	i2c_write(ssd1306_sla, data, 2);
}

void ssd1306_init(const uint8_t sla)
{
	ssd1306_sla = sla;

	ssd1306_clearbuf();

	ssd1306_command(SSD1306_DISPLAYOFF);
	ssd1306_command(SSD1306_SETDISPLAYCLOCKDIV);
	ssd1306_command(0x80);
	ssd1306_command(SSD1306_SETMULTIPLEX);
	ssd1306_command(SSD1306_LCDHEIGHT - 1);
	ssd1306_command(SSD1306_SETDISPLAYOFFSET);
	ssd1306_command(0x00);
	ssd1306_command(SSD1306_SETSTARTLINE | 0x00);
	ssd1306_command(SSD1306_CHARGEPUMP);
	ssd1306_command(0x14);
	ssd1306_command(SSD1306_MEMORYMODE);
	ssd1306_command(0x00);
	ssd1306_command(SSD1306_SEGREMAP | 0x01);
	ssd1306_command(SSD1306_COMSCANDEC);

	ssd1306_command(SSD1306_SETCOMPINS);
#if SSD1306_LCDHEIGHT == 32
	ssd1306_command(0x02);
#elif SSD1306_LCDHEIGHT == 64
	ssd1306_command(0x12);
#else
#error No mapping for SSD1306 module
#endif
	ssd1306_setcontrast(0x40);

	ssd1306_command(SSD1306_SETPRECHARGE);
	ssd1306_command(0xF1);
	ssd1306_command(SSD1306_SETVCOMDETECT);
	ssd1306_command(0x40);
	ssd1306_command(SSD1306_DISPLAYALLON_RESUME);
	ssd1306_command(SSD1306_DEACTIVATE_SCROLL);
	ssd1306_command(SSD1306_DISPLAYON);
}

void ssd1306_clear(void)
{
	ssd1306_clearbuf();
}

void ssd1306_display(void)
{
	ssd1306_command(SSD1306_COLUMNADDR);
	ssd1306_command(SSD1306_COLUMN_START_ADDR);
	ssd1306_command(SSD1306_COLUMN_END_ADDR);
	ssd1306_command(SSD1306_PAGEADDR);
	ssd1306_command(SSD1306_PAGE_START_ADDR);
	ssd1306_command(SSD1306_PAGE_END_ADDR);

	uint8_t txb[1 + 16] = {0x40};
	uint8_t *dest = txb + 1;
	uint8_t *src;
	uint8_t i;

	for (i = 0; i < SSD1306_BUFSIZE / 16; i++) {
		src = ssd1306_buffer + (i * 16);
		my_memcpy(dest, src, 16);
		i2c_write(ssd1306_sla, txb, 1 + 16);
	}
}

#define MOD8(n) (n & 7)

void ssd1306_setpixel(const uint8_t x, const uint8_t y, const uint8_t state)
{
	uint16_t i = x + (y / 8) * SSD1306_LCDWIDTH;
	uint8_t byte = (1 << MOD8(y));

	if (state)
		ssd1306_buffer[i] |=  byte;
	else
		ssd1306_buffer[i] &= ~byte;
}

void ssd1306_drawchar(const uint8_t x, const uint8_t y, const char c)
{
	uint8_t i;
	uint8_t j;
	uint8_t line;
	uint8_t state;

	for (i = 0; i < 5; i++) {
		line = pgm_read_byte(&font[c * 5 + i]);
		for (j = 0; j < 8; j++) {
			state = line & 1;
			ssd1306_setpixel(x + i, y + j, state);
			line >>= 1;
		}
	}
}

void ssd1306_write(const uint8_t x, const uint8_t y, char *s)
{
	uint8_t i = 0;
	while (*s) {
		ssd1306_drawchar(x + (i++ * (5 + 1)), y, *s++);
	}
}

#if 0
void ssd1306_drawrect(const uint8_t x, const uint8_t y,
			const uint8_t width, const uint8_t height)
{
	uint8_t x2, y2;

	/* draw top and bottom line */
	for (x2 = x; x2 < x + width; x2++) {
		ssd1306_setpixel(x2, y, 1);
		ssd1306_setpixel(x2, y + height, 1);
	}
	for (y2 = y; y2 < y + height; y2++) {
		ssd1306_setpixel(x, y2, 1);
		ssd1306_setpixel(x + width, y2, 1);
	}
}
#endif

void ssd1306_setcontrast(const uint8_t v)
{
	ssd1306_command(SSD1306_SETCONTRAST);
	ssd1306_command(v);
}
