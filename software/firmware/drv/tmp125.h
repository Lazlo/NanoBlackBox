#pragma once

#include <stdint.h>

void tmp125_init(void);
int16_t tmp125_read(void);
void tmp125_tostr(int16_t c, char *s);
