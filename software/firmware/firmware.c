#include "config.h"
#include "config_pins.h"
#include "gpio.h"
#include "reset.h"
#include "uart.h"
#include "spi.h"
#include "tmp125.h"
#include "graph.h"
#include "i2c.h"
#include "ssd1306.h"
#include "uptime.h"
#include "button.h"
#include "sched.h"
#include "timer.h"
#include <avr/sleep.h>
#include <avr/interrupt.h>

#define ARR_LEN(a)	(sizeof(a)/sizeof(a[0]))

static const struct gpio_struct gpio_list[] = {
	{
		.offset		= PIN_BTN1,
		.direction_reg	= &PIN_BTN1_DDR,
		.output_reg	= &PIN_BTN1_PORT,
		.input_reg	= &PIN_BTN1_PIN
	},
	{
		.offset		= PIN_BTN2,
		.direction_reg	= &PIN_BTN2_DDR,
		.output_reg	= &PIN_BTN2_PORT,
		.input_reg	= &PIN_BTN2_PIN
	},
	{
		.offset		= PIN_BTN3,
		.direction_reg	= &PIN_BTN3_DDR,
		.output_reg	= &PIN_BTN3_PORT,
		.input_reg	= &PIN_BTN3_PIN
	},
	{
		.offset		= PIN_BTN4,
		.direction_reg	= &PIN_BTN4_DDR,
		.output_reg	= &PIN_BTN4_PORT,
		.input_reg	= &PIN_BTN4_PIN
	}
};

static struct button_struct button_list[] = {
	{.pin = &gpio_list[0]},
	{.pin = &gpio_list[1]},
	{.pin = &gpio_list[2]},
	{.pin = &gpio_list[3]}
};

static char uptime_str[9];
static int16_t temp;

#define TG_BUF_LEN	60
static uint8_t tg_buf[TG_BUF_LEN];
static struct graph_struct tg;

static char temp_str[7];
static char temp_max_str[7];
static char temp_min_str[7];

static uint8_t func_state[4] = {0, 0, 0, 0};

static void uptime_task(void)
{
	uptime_tick();
	uptime_get(uptime_str);
	uart_puts(uptime_str);
	uart_putc(' ');

	temp = tmp125_read();

	graph_add(&tg, temp);

	tmp125_tostr(temp, temp_str);
	temp_str[4] = ' ';
	temp_str[5] = 'C';
	temp_str[6] = '\0';
	tmp125_tostr(tg.max, temp_max_str);
	temp_max_str[4] = ' ';
	temp_max_str[5] = 'C';
	temp_max_str[6] = '\0';
	tmp125_tostr(tg.min, temp_min_str);
	temp_min_str[4] = ' ';
	temp_min_str[5] = 'C';
	temp_min_str[6] = '\0';

	uart_puts(temp_str);

	uart_puts(" (max ");
	uart_puts(temp_max_str);
	uart_puts(" min ");
	uart_puts(temp_min_str);
	uart_puts(")");
	uart_puts("\r\n");
}

static uint8_t ssd1306_initialized = 0;

static void ssd1306_task(void)
{
	if (!ssd1306_initialized) {
		ssd1306_init(SSD1306_SLA);
		ssd1306_initialized = 1;
	}

	ssd1306_write(0, 0, uptime_str);
	ssd1306_write(72, 0, temp_str);
	ssd1306_write(111, 0, "now");
	ssd1306_write(72, 8, temp_max_str);
	ssd1306_write(111, 8, "max");
	ssd1306_write(72, 16, temp_min_str);
	ssd1306_write(111, 16, "min");

	graph_draw(&tg, 0, 8,
			SSD1306_LCDWIDTH - ((5 * 6) + 1), SSD1306_LCDHEIGHT / 2, ssd1306_setpixel);
	ssd1306_display();

	ssd1306_clear();
}

static void button_task(void)
{
	uint8_t i;
	struct button_struct *button;
	char str[] = "btnN";

	button_update(button_list, ARR_LEN(button_list));

	for (i = 0; i < ARR_LEN(button_list); i++) {
		button = &button_list[i];
		if (button->changed == 0)
			continue;
		button->changed = 0;

		if (button->state == 0)
			func_state[i] = func_state[i] ? 0 : 1;

		str[3] = '1' + i;
		uart_puts(str);
		uart_puts(" = ");
		uart_putc('0' + button->state);
		uart_puts("\r\n");
	}

	for (i = 0; i < ARR_LEN(button_list); i++) {
		if (i == 0) {
			if (func_state[i] == 1) {
				PIN_BEEPER_PORT &= ~(1 << PIN_BEEPER);
			} else {
				PIN_BEEPER_PORT |= (1 << PIN_BEEPER);
			}
		}
		if (func_state[i]) {
			str[3] = '1' + i;
#if 1
			ssd1306_write((SSD1306_LCDWIDTH / 4) * (3 - i), 24, str);
#else
			ssd1306_write((128/4) * (3 - i), 22, str);
			ssd1306_drawrect((128/4) * (3 - i) -2, 20, 4 * 6 + 2, 7 + (2 * 2));
#endif
		}
	}
}

static void print_reset_cause(void) {
	uint8_t reset_cause = reset_get();

	uart_puts("RST");
	if (reset_cause & (1 << 0)) {
		uart_puts(" POR");
	}
	if (reset_cause & (1 << 1)) {
		uart_puts(" EXTR");
	}
	if (reset_cause & (1 << 2)) {
		uart_puts(" BOR");
	}
	if (reset_cause & (1 << 3)) {
		uart_puts(" WDR");
	}
	uart_puts("\r\n");
}

int main(void)
{
	PIN_BEEPER_DDR |= (1 << PIN_BEEPER);
	PIN_BEEPER_PORT |= (1 << PIN_BEEPER);

	uart_init(UART_BAUD);
	uart_puts("UART ready!\r\n");
	print_reset_cause();
	spi_init();
	tmp125_init();
	graph_init(&tg, tg_buf, TG_BUF_LEN);
	i2c_init(I2C_F_SCL);
	uptime_init();
	button_init(button_list, ARR_LEN(button_list));
	sched_init();
	sched_addtask(button_task, 50, 0);
	sched_addtask(uptime_task, 1000, 0);
	sched_addtask(ssd1306_task, 250, 0);
	timer2_init();
	timer2_sethandler(sched_update);
	timer2_enable();
	set_sleep_mode(SLEEP_MODE_IDLE);
	sei();
	while (1) {
		sched_dispatch();
		sleep_mode();
	}
	return 0;
}
