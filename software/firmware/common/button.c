#include "button.h"
#include "gpio.h"

void button_init(struct button_struct button_list[], const uint8_t len)
{
	uint8_t i;
	struct button_struct *button;

	/* NOTE Pins are inputs by default - no need to configure direction */

	for (i = 0; i < len; i++) {
		button = &button_list[i];
		button->changed = 0;
		button->debounce = 0;
		/* Enable pull-up resistors */
		gpio_write(button->pin, 1);
		/* Perform initial read */
		button->state_last = button->state = gpio_read(button->pin);
	}
}

void button_update(struct button_struct button_list[], const uint8_t len)
{
	const uint8_t debounce_max = 4;
	uint8_t i;
	struct button_struct *button;

	for (i = 0; i < len; i++) {
		button = &button_list[i];
		button->state = gpio_read(button->pin);
		/* When there was a change, re-start the debounce process */
		if (button->state != button->state_last) {
			button->state_last = button->state;
			button->debounce = 1;
			continue;
		}
		/* When states are equal, but no debounce
		 * procedure has been started, continue. */
		if (!button->debounce)
			continue;
		/* When states are equal, but debounce count
		 * is not met, continue */
		if (++button->debounce < debounce_max)
			continue;
		button->debounce = 0;
		button->changed = 1;
	}
}
