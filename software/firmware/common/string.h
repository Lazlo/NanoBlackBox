#pragma once

#include <stdint.h>

void my_memcpy(uint8_t *dest, const uint8_t *src, uint16_t len);
void my_memset(uint8_t *s, uint8_t c, const uint16_t n);
