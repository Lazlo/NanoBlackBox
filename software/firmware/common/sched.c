#include "sched.h"
#include "config.h"

struct sched_task_struct {
	void (*fp)(void);
	uint16_t period;
	uint16_t delay;
	uint8_t run;
};

static struct sched_task_struct sched_task_list[SCHED_TASKS_MAX];

static void sched_cleartask(struct sched_task_struct *t)
{
	t->fp = 0;
	t->period = 0;
	t->delay = 0;
	t->run = 0;
}

void sched_init(void)
{
	uint8_t i;
	for (i = 0; i < SCHED_TASKS_MAX; i++)
		sched_cleartask(&sched_task_list[i]);
}

uint8_t sched_addtask(void (*fp)(void), const uint16_t period, const uint16_t delay)
{
	struct sched_task_struct *t;
	uint8_t i;
	for (i = 0; i < SCHED_TASKS_MAX; i++) {
		if (sched_task_list[i].fp)
			continue;
		break;
	}
	if (i == SCHED_TASKS_MAX)
		return 0;
	t = &sched_task_list[i];
	t->fp = fp;
	t->period = period;
	t->delay = delay;
	return 1 + i;
}

void sched_update(void)
{
	struct sched_task_struct *t;
	uint8_t i;
	for (i = 0; i < SCHED_TASKS_MAX; i++) {
		t = &sched_task_list[i];
		if (!t->fp)
			continue;
		if (t->delay-- > 0)
			continue;
		t->run += 1;
		if (t->period)
			t->delay = t->period;
	}
}

void sched_dispatch(void)
{
	struct sched_task_struct *t;
	uint8_t i;
	for (i = 0; i < SCHED_TASKS_MAX; i++) {
		t = &sched_task_list[i];
		if (!t->fp)
			continue;
		if (!t->run)
			continue;
		(*t->fp)();
		t->run -= 1;
	}
}
