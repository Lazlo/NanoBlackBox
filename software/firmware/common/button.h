#pragma once

#include "gpio.h"

struct button_struct {
	const struct gpio_struct *pin;
	uint8_t changed;
	uint8_t state_last;
	uint8_t state;
	uint8_t debounce;
};

void button_init(struct button_struct button_list[], const uint8_t len);
void button_update(struct button_struct button_list[], const uint8_t len);
