#include "string.h"

void my_memcpy(uint8_t *dest, const uint8_t *src, uint16_t len)
{
	uint16_t i;
	for (i = 0; i < len; i++)
		dest[i] = src[i];
}

void my_memset(uint8_t *s, uint8_t c, const uint16_t n)
{
	uint16_t i;
	for (i = 0; i < n; i++)
		s[i] = c;
}
