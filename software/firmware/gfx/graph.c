#include "graph.h"

void graph_init(struct graph_struct *g, uint8_t *buf, const uint8_t len)
{
	g->buf = buf;
	g->len = len;
	g->i = 0;
	g->min = g->max = 0;
	g->initialized = 0;
}

void graph_add(struct graph_struct *g, const int16_t v)
{
	if (g->initialized == 0)
	{
		g->initialized = 1;
		g->min = g->max = v;
	}
	else
	{
		if (g->min > v)
			g->min = v;
		if (g->max < v)
			g->max = v;
	}

	g->buf[g->i] = v;
	if (++g->i == g->len)
		g->i = 0;
}

void graph_draw(struct graph_struct *g,
		uint8_t x, uint8_t y, const uint8_t width, const uint8_t height,
		void (*draw)(const uint8_t, const uint8_t, const uint8_t))
{
	uint8_t original_x = x;
	uint8_t original_y = y;
	uint8_t y_offset = y + (height / 4) * 3;

	for (; x < g->i && x < (original_x + width); x++)
	{
		for (y = y_offset - (g->buf[x] - g->min); y < original_y + height; y++)
			(*draw)(x, y, 1);
	}
}
