#pragma once

#include <stdint.h>

struct graph_struct {
	uint8_t *buf;
	uint8_t len;
	uint8_t i;
	int16_t min;
	int16_t max;
	uint8_t initialized;
};

void graph_init(struct graph_struct *g, uint8_t *buf, const uint8_t len);
void graph_add(struct graph_struct *g, const int16_t v);
void graph_draw(struct graph_struct *g,
		uint8_t x, uint8_t y, const uint8_t width, const uint8_t height,
		void (*draw)(const uint8_t, const uint8_t, const uint8_t));
