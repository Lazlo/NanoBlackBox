# NanoBlackBox

Is a little plastic box, containing:

 * an ATMEL AVR ATmega8 (on a board with Arduino Nano pinout)
 * a [128 by 32 pixel OLED display](http://bit.ly/2ijiCuD) (SSD1306)
 * four push-buttons
 * a temperature sensor (TI TMP125)
 * a 8kByte EEPROM (25LC640)

## Firmware

[![Build Status](https://travis-ci.org/lazlo/nanoblackbox.svg?branch=master)](https://travis-ci.org/lazlo/nanoblackbox)
